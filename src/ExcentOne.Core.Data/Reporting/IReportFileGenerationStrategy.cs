﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ExcentOne.Core.Data.Reporting
{
    public interface IReportFileGenerationStrategy
    {
        Stream GenerateReportFile(IEnumerable records, string fileName = null);

        Type ModelType { get; }
        IEnumerable<string> FileExtensions { get; }
    }
}