﻿using System.Collections.Generic;
using System.IO;

namespace ExcentOne.Core.Data.Reporting
{
    public interface IReportFileGenerationStrategy<T> : IReportFileGenerationStrategy where T : class
    {
        Stream GenerateReportFile(IEnumerable<T> records, string fileName = null);
    }
}