﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Linq;
using System.Reflection;

// ReSharper disable InconsistentNaming
namespace ExcentOne.Core.Data.Extraction
{
    public class DataExtractor : IDataExtractor
    {
        private const string EXT_DATA_ERROR_0001 = "No {0} has been found for {1} file.";

        [ImportMany(typeof(DataExtractionStrategy))]
        private readonly List<DataExtractionStrategy> _strategies = new List<DataExtractionStrategy>();

        public DataExtractor()
        {
            ComposeStrategies(Assembly.GetCallingAssembly());
        }

        private void ComposeStrategies(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetExecutingAssembly();
            var catalog = new AssemblyCatalog(assembly);
            var container = new CompositionContainer(catalog);
            container.SatisfyImportsOnce(this);
        }

        #region Implemented methods

        public bool CanExtractFrom(string fileName)
        {
            return _strategies.Any(strategy => strategy.CanExtract(fileName));
        }

        public virtual ICollection<TRecord> Extract<TRecord>(Stream fileStream, string fileName)
            where TRecord : class, new()
        {
            return Extract<TRecord>(
                fileStream,
                fileName,
                _strategies.Find(s => s.CanExtract(fileName)) ??
                throw new InvalidOperationException(string.Format(
                    EXT_DATA_ERROR_0001,
                    nameof(DataExtractionStrategy),
                    Path.GetExtension(fileName))));
        }

        public ICollection<TRecord> Extract<TRecord>(
            Stream fileStream, string fileName, DataExtractionStrategy strategy)
            where TRecord : class, new()
        {
            fileStream = fileStream ?? throw new ArgumentNullException(nameof(fileStream));
            fileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
            strategy = strategy ?? throw new ArgumentNullException(nameof(strategy));

            return strategy.ExtractFrom<TRecord>(fileStream, fileName);
        }

        #endregion Implemented methods
    }
}