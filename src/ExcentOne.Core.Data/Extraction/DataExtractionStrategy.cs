﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace ExcentOne.Core.Data.Extraction
{
    /// <summary>
    /// Base class for implementing a strategy for extracting data records from a file.
    /// </summary>
    [DebuggerDisplay(nameof(DebugView))]
    [InheritedExport(typeof(DataExtractionStrategy))]
    public abstract class DataExtractionStrategy
    {
        protected DataExtractionStrategy(params string[] fileExtensions)
        {
            FileExtensions = fileExtensions is null || fileExtensions.Length == 0 ?
                SetAcceptableFileExtensions() : fileExtensions;
        }

        /// <summary>
        /// An array of file extension supported by the current <see cref="DataExtractionStrategy"/>.
        /// </summary>
        protected internal string[] FileExtensions { get; }

        /// <summary>
        /// Checks whether the current <see cref="DataExtractionStrategy"/> can extract data
        /// from the file.
        /// </summary>
        /// <param name="fileName">The name of the file.</param>
        /// <returns><c>true</c> if the current <see cref="DataExtractionStrategy"/> can extract data from file; otherwise false.</returns>
        protected internal bool CanExtract(string fileName)
        {
            fileName = fileName ?? throw new ArgumentNullException(nameof(fileName));
            return FileExtensions
                .Any(ext => fileName.EndsWith(ext, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// Extracts a collection of <typeparamref name="TRecord"/>s from the specified file.
        /// </summary>
        /// <typeparam name="TRecord">The type of record.</typeparam>
        /// <param name="stream">The file stream of the target file.</param>
        /// <param name="fileName">The name of the file.</param>
        /// <returns>A collection of <typeparamref name="TRecord"/>s extracted from the file.</returns>
        protected internal abstract ICollection<TRecord> ExtractFrom<TRecord>(
            Stream stream, string fileName)
            where TRecord : class, new();

        private string[] SetAcceptableFileExtensions()
        {
            var type = GetType();
            return new[] { type.Name.Replace(nameof(DataExtractionStrategy), "").ToLowerInvariant() };
        }

        protected virtual string DebugView
            => $"{GetType().Name} ({string.Join(". ", FileExtensions.Select(e => $"\".{e}\"").ToArray())})";
    }
}