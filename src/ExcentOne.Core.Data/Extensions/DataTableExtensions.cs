﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;

namespace ExcentOne.Core.Data.Extensions
{
    public static class DataTableExtensions
    {
        private static List<Type> _systemTypes;

        public static List<Type> SystemTypes =>
            _systemTypes ?? (_systemTypes = Assembly.GetExecutingAssembly().GetType().Module.Assembly
                .GetExportedTypes().ToList());

        [SuppressMessage("ReSharper", "NotResolvedInText")]
        public static DataTable ToDataTable<T>(this T obj, bool isCreateColumns = false, bool isIncludeData = true,
            bool isExpandProperties = false)
        {
            var dataTable = new DataTable();
            if (obj == null) throw new ArgumentNullException(@"Cannot convert null object to DataTable");

            if (isCreateColumns) CreateDataTableColumns(obj, dataTable, isExpandProperties);

            if (!isIncludeData) return dataTable;
            foreach (var item in (IEnumerable) obj)
            {
                var row = dataTable.NewRow();
                foreach (var prop in item.GetType().GetProperties())
                    if (!SystemTypes.Contains(prop.PropertyType) && isExpandProperties)
                    {
                        var cProp = prop.GetValue(item, null);
                        if (cProp == null) continue;
                        foreach (var inProp in cProp.GetType().GetProperties())
                            SetDataRowFieldValue(row, $"{prop.Name}{inProp.Name}", inProp.GetValue(cProp, null));
                    }
                    else
                    {
                        SetDataRowFieldValue(row, prop.Name, prop.GetValue(item, null));
                    }

                dataTable.Rows.Add(row);
            }

            dataTable.AcceptChanges();

            return dataTable;
        }

        private static void SetDataRowFieldValue(DataRow row, string columnName, object value)
        {
            if (row.Table.Columns.Contains(columnName)) row[$"{columnName}"] = value;
        }

        private static void CreateDataTableColumns<T>(T obj, DataTable dataTable, bool isExpandProperties)
        {
            foreach (var item in (IEnumerable) obj)
            {
                foreach (var prop in item.GetType().GetProperties())
                    if (!SystemTypes.Contains(prop.PropertyType) && isExpandProperties)
                        CreateDataTableColumns<T>(dataTable, prop);
                    else
                        dataTable.Columns.Add(prop.Name, typeof(string));

                dataTable.AcceptChanges();
                return;
            }
        }

        [SuppressMessage("ReSharper", "SuggestBaseTypeForParameter")]
        [SuppressMessage("ReSharper", "UnusedTypeParameter")]
        private static void CreateDataTableColumns<T>(DataTable dataTable, PropertyInfo prop)
        {
            if (prop.DeclaringType == null) return;
            var propertyInfos = prop.DeclaringType.GetProperties();
            foreach (var inProp in propertyInfos)
                if (!prop.Name.Equals(inProp.Name))
                    dataTable.Columns.Add($"{prop.Name}{inProp.Name}", typeof(string));
        }

        public static bool HasRows(this DataTable dt)
        {
            return dt?.Rows != null && dt.Rows.Count > 0;
        }
        public static DataRow GetTopRow(this DataTable dt)
        {
            return dt.HasRows() ? dt.Rows[0] : null;
        }

        public static List<T> ToList<T>(this DataTable dt) where T : class, new()
        {
            var data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                var item = row.ToObject<T>(); //GetItem<T>(row);
                data.Add(item);
            }

            return data;
        }

        [SuppressMessage("ReSharper", "RedundantJumpStatement")]
        [SuppressMessage("ReSharper", "UnusedMember.Local")]
        private static T GetItem<T>(DataRow dr)
        {
            var temp = typeof(T);
            var obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            foreach (var pro in temp.GetProperties())
                if (pro.Name == column.ColumnName)
                    pro.SetValue(obj, dr[column.ColumnName], null);
                else
                    continue;
            return obj;
        }
    }
}